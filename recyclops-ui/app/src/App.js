import React, { Component } from 'react';
import './App.css';

import Toast from 'light-toast';
import Webcam from "react-webcam";


const videoConstraints = {
  width: 1280,
  height: 720,
  facingMode: "environment"
};


class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      processing: false
    };
  }

  showPrediction = (clazz) => {
    Toast.info(clazz, 1000);
    this.setState({ processing: !this.state.processing })  
  };

  handleClick = () => {
    if (this.state.processing) 
      return;

    this.setState({ processing: !this.state.processing })  
    Toast.loading('Analyzing image...');
    const screenshot = this.webcam.getScreenshot().split(',')[1];  // split to ignore base64 metadata

    this.sendPhoto(screenshot).then(response => {
      Toast.hide();
      this.showPrediction(response.prediction)
    });
  }

  sendPhoto = (photo) => {
    return fetch('/classifier', {
      method: 'POST', 
      mode: 'cors', 
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }, 
      body: JSON.stringify({
        'image': photo
      })
    }).then(response => {
      if (!response.ok) {
        console.log(`Unsuccessful response calling back-end`)
      }
      return response.json()
    }).catch(error => {
      console.log(`Exception calling back-end service ${error}`)
    })
  }

  render() {
    return (
      <div>
        <Webcam
          className="camera-component"
          audio={false}
          height={800}
          ref={node => this.webcam = node}
          screenshotFormat="image/jpeg"
          width={600}
          videoConstraints={videoConstraints}
        />
        <div className="btn-holder">
          <button className="simple-btn" onClick={this.handleClick}></button>
        </div>
      </div>
    );
  }
}
  
export default App;
