import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('verifies app renders', () => {
  const { getByText } = render(<App />);
  expect(getByText).not.toBeNull();
});
