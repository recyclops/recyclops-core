import io
import pathlib

import tensorflow as tf
import numpy as np
import base64
import cv2

from PIL import Image


class Classifier:

    def __init__(self, model_name='latest'):
        self._model = Classifier._load_model('{}/ai/models/{}.hdf5'.format(pathlib.Path().absolute(), model_name))
        self._classes = ['cardboard', 'glass', 'metal', 'paper', 'plastic', 'trash']

    def classify(self, image):
        return self._generate_prediction(Classifier._normalize_image(image))

    def _generate_prediction(self, image):
        return self._classes[int(np.argmax(self._model.predict(image)[0]))]

    @staticmethod
    def _load_model(fp):
        return tf.keras.models.load_model(fp)

    @staticmethod
    def _normalize_image(image):
        norm = np.array(Image.open(io.BytesIO(base64.b64decode(image)))).astype('float32') * (1. / 255)
        return np.reshape(cv2.resize(norm, dsize=(512, 384)), (1, 512, 384, 3))
