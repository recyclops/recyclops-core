from ai import Classifier
import config

from flask import Flask, request, jsonify
from flask_cors import CORS


def create_app(app_config=config.get_config()):
    app = Flask(__name__)
    app.config.from_object(app_config)
    CORS(app)

    classifier = Classifier()

    @app.route('/classifier', methods=['POST'])
    def classify_waste():
        r = request.json['image']
        return jsonify({
            'prediction': classifier.classify(r)
        })

    return app


if __name__ == '__main__':
    main_config = config.get_config()
    create_app(main_config).run(host=main_config.HOST, port=main_config.PORT)
