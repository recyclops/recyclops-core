import os
import config.settings


def get_config(app_env=os.environ.get('APP_ENV', 'dev')):
    return config.settings.config_by_env[app_env]
