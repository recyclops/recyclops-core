class Config:
    DEBUG = True
    TESTING = True
    PORT = 5000
    HOST = '0.0.0.0'


class ProductionConfig(Config):
    DEBUG = False
    TESTING = False
    PORT = 80


class DevelopmentConfig(Config):
    DEBUG = True
    TESTING = False


class TestingConfig(Config):
    DEBUG = True
    TESTING = True


config_by_env = dict(
    dev=DevelopmentConfig,
    prod=ProductionConfig,
    testing=TestingConfig
)
