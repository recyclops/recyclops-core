from ai import Classifier

import unittest
import base64

from io import BytesIO
from PIL import Image


class AiTest(unittest.TestCase):

    def setUp(self) -> None:
        test_image = Image.open('test/resources/glass.jpg')
        buffered = BytesIO()
        test_image.save(buffered, format="JPEG")
        img_str = base64.b64encode(buffered.getvalue())

        self.test_image = img_str

    def test_normalize_image(self):
        # Arrange
        classifier = Classifier()

        # Act
        normalized_image = classifier._normalize_image(self.test_image)

        # Assert
        self.assertIsNotNone(normalized_image)
        self.assertEqual((1, 512, 384, 3), normalized_image.shape)
        self.assertTrue(normalized_image.min() >= 0 and normalized_image.max() <= 1)

    def test_constructor(self):
        classifier = Classifier()

        # Assert
        self.assertIsNotNone(classifier._model)
        self.assertListEqual(classifier._classes, ['cardboard', 'glass', 'metal', 'paper', 'plastic', 'trash'])

    def test_classify(self):
        # Arrange
        classifier = Classifier()

        # Act
        prediction = classifier.classify(self.test_image)

        # Assert
        self.assertEqual(prediction, 'glass')

    def test_generate_prediction(self):
        # Arrange
        classifier = Classifier()

        # Act
        prediction = classifier._generate_prediction(classifier._normalize_image(self.test_image))

        # Assert
        self.assertEqual(prediction, 'glass')
