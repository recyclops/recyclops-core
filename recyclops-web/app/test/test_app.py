import app
import unittest
from config import get_config

from PIL import Image
import base64
from io import BytesIO
import json


class AppTest(unittest.TestCase):

    def test_create_app(self):
        self.assertIsNotNone(app.create_app())

    def test_smoke_classification(self):
        # Arrange
        test_image = Image.open('test/resources/glass.jpg')
        buffered = BytesIO()
        test_image.save(buffered, format="JPEG")
        img_str = base64.b64encode(buffered.getvalue())
        test_client = app.create_app(get_config('testing')).test_client()

        # Act
        response = test_client.post('/classifier', json={
            'image': img_str.decode('utf-8')
        })

        # Assert
        self.assertEqual(200, response.status_code)
        data = json.loads(response.get_data(as_text=True))
        self.assertTrue(data['prediction'] in ['cardboard', 'glass', 'metal', 'paper', 'plastic', 'trash'])
