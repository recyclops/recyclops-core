import unittest
from config import get_config
from config.settings import DevelopmentConfig, TestingConfig, ProductionConfig, config_by_env


class ConfigTest(unittest.TestCase):

    def test_create_config(self):
        self.assertIsNotNone(get_config())

    def test_get_dev_config(self):
        self.assertIs(get_config('dev'), DevelopmentConfig)

    def test_get_test_config(self):
        self.assertIs(get_config('testing'), TestingConfig)

    def test_get_prod_config(self):
        self.assertIs(get_config('prod'), ProductionConfig)

